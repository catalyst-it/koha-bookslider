# Introduction

The BookSlider plugin allows you to generate a slideshow of cover images from either a Koha list or report.

Simply install and enable the plugin, choose a public list or report, and generate the slider. Slider markup is automatically printed to all OpacMainUserBlock news items.

To refresh the slider, in order to show new biblios returned from the report or added to a list, you need to regenerate the slider.

# Downloading

* Go to the Gitlab repository, choose the 'main' branch if you're running Koha 18.11 - 20.11
* Download the KPZ file

# Installing

* Change `<enable_plugins>0<enable_plugins>` to `<enable_plugins>1</enable_plugins>` in your koha-conf.xml file
* Confirm that the path to <pluginsdir> exists, is correct, and is writable by the web server
* Restart your webserver
* Restart memcached if you are using it
* Go to Koha Administration > Manage plugins 
* Upload the KPZ file 

# Configure the OpacMainUserBlock and OpacUserJS

If you have content already in the OpacMainUserBlock and OpacUserJS then do the following:

**OpacMainUserBlock:**
* Go to Tools > News
* Edit all instances of the OpacMainUserBlock news item
* If using the 'WYSIWYG editor' then click on the '<>' (source code), otherwise if using 'text editor' you can just straight paste in the following tags in the position you want the slider to be displayed in:
```
<!-- Begin bookslide -->
<!-- End bookslide -->
```
* Click 'Submit'

**OpacUserJS:**
* Go to: Koha Administration > Global system preferences
* Search for: OpacUserJS
* Paste in the following tags:

```
<!-- Begin bookslide -->
<!-- End bookslide -->
```

* Click Save

_**CAUTION**: If you do not add the above two slider tags into the OpacMainUserBlock news item and OpacUserJS system preference then all existing content i both will be overwritten when you generate the slider._

# Enable cover image system preferences

You need to have at least one of the following system preferences installed for the plugin to be able to fetch any cover images:

* OPACAmazonCoverImages
* OpacCoce/CoceHost/CoceProviders
* GoogleJackets
* SyndeticsCoverImages/SyndeticsClientCode

# Run the plugin

Once the plugin is installed stay in Koha Administration > Manage plugins, and enable the plugin:

* Click: Actions > Enable 
* Click: Actions > Run tool 
* Either choose a public list from the 'Select a list' dropdown OR enter a report ID as your source of books
* Either choose to generate the slider using default options or choose different options
* Click 'Generate'



