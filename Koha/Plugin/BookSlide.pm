package Koha::Plugin::BookSlide;
# Mehdi Hamidi, 2016 - InLibro
# Modified by Bouzid Fergani, 2016 - InLibro
# Modified by Liz Rea, 2018 - Catalyst IT NZ
#
# This plugin allows you to generate a BookSlide of books from available lists
# and insert the template into either the table system preferences;OpacMainUserBlock
# or table opac_news;lang LIKE OpacMainUserBlock
#
# This file is part of Koha.
#
# Koha is free software; you can redistribute it and/or modify it under the
# terms of the GNU General Public License as published by the Free Software
# Foundation; either version 3 of the License, or (at your option) any later
# version.
#
# Koha is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE.  See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with Koha; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
use Modern::Perl;
use strict;
use CGI;
use DBI;
use JSON qw( decode_json );
use LWP::Simple;
use Template;
use utf8;
use base qw(Koha::Plugins::Base);
use C4::Auth;
use C4::Biblio;
use C4::Context;
use C4::Koha qw(GetNormalizedISBN);
use C4::Output;
use C4::Reports::Guided;
use Koha::Reports;
use C4::XSLT;
use C4::NewsChannels;

our $VERSION = 2.1;
our $metadata = {
    name            => 'BookSlide',
    author          => 'Catalyst IT, LTD',
    description     => 'Generates a book slider from available lists, or from a specified report',
    date_authored   => '2018-09-20',
    date_updated    => '2021-07-07',
    minimum_version => '18.05',
    maximum_version => undef,
    version         => $VERSION,
};

our @shelves;
our $dbh = C4::Context->dbh();

sub new {
    my ( $class, $args ) = @_;
    $args->{'metadata'} = $metadata;
    $args->{'metadata'}->{'class'} = $class;
    my $self = $class->SUPER::new($args);

    return $self;
}

sub tool {
    my ( $self, $args ) = @_;
    my $cgi = $self->{'cgi'};

    @shelves =  Koha::Virtualshelves->search(undef,
        { order_by => { -asc => 'shelfname' }
    });
    my $error;
    if ($cgi->param('action') && ( $cgi->param('selectedShelf') || $cgi->param('report_id')) ){
        my $selectedShelf = $cgi->param('selectedShelf');
        my $report_id = $cgi->param('report_id');
        my $slidesToShow = $cgi->param('slidesToShow');
        my $autoplay = $cgi->param('autoplay') || "false";
        my $arrows = $cgi->param('arrows') || "false";
        my $sliderSpeed = $cgi->param('sliderSpeed');
        my $showTitleAndAuthor = "true" if ($cgi->param('showTitleAndAuthor'));
        my $checkflag = "true" if ($cgi->param('flag'));
        my $flagtext = $cgi->param('flagtext') || "";
        $self->generateSlider($selectedShelf, $report_id, $slidesToShow, $autoplay, $arrows, $sliderSpeed, $showTitleAndAuthor, $checkflag, $flagtext );
        $self->go_home();
    }elsif ($cgi->param('action') && (!$cgi->param('selectedShelf') || !$cgi->param('report_id')) ){
        $error = 1;
        $self->step_1($error);
    } else {
        $self->step_1();
    }

}

sub step_1{
    my ( $self, $error) = @_;
    my $cgi = $self->{'cgi'};
    #Choose the language to use
    my $preferedLanguage = $cgi->cookie('KohaOpacLanguage');
    #English is the default language, make a template if you want another language
    my $template = undef;
    if ($preferedLanguage) {
        eval {$template = $self->get_template( { file => "step_1_" . $preferedLanguage . ".tt" } )};
        if(!$template){
            $preferedLanguage = substr $preferedLanguage, 0, 2;
            eval {$template = $self->get_template( { file => "step_1_$preferedLanguage.tt" } )};
        }
    }
    $template = $self->get_template( { file => 'step_1.tt' } ) unless $template;

    $template->param(shelves => \@shelves, selectedShelf => $self->retrieve_data('selectedShelf'), error => $error, );
    print $cgi->header(-type => 'text/html',-charset => 'utf-8');
    print $template->output();
}

sub generateSlider{
    my ( $self, $selectedShelf, $report_id, $slidesToShow, $autoplay, $arrows, $sliderSpeed, $showTitleAndAuthor, $checkflags, $flagtext ) = @_;
    my $shelf;
    my $sqllist;
    my @contents = ();
    my $shelfid;
    my $shelfname;
    my $sql;
    my @items;
    my @images;
    my $flagged = 0;

    if ($selectedShelf && !$report_id) {
        foreach my $list (@shelves){
            $shelf = $list if($list->shelfnumber == $selectedShelf);
        }
        $shelfid = $shelf->shelfnumber;

        #content : shelfnumber,biblionumber,flags,dateadded, borrowernumber
        @contents = Koha::Virtualshelfcontents->search({shelfnumber => $shelf->shelfnumber});
        $shelfname = $shelf->shelfname;
        $shelfname =~ s/[^a-zA-Z0-9]/_/g;
        foreach my $content (@contents){
            my %items;
            $items{biblionumber} = $content->biblionumber;
            push @items,\%items;
        }
    } elsif (!$selectedShelf && $report_id ) {
        # do stuff with the saved report
       if (my $report = Koha::Reports->find($report_id)) {
          $sql = $report->savedsql;
          my $report_sth = $dbh->prepare($sql);
          $report_sth->execute or die $report_sth->errstr;
          while (my $item = $report_sth->fetchrow_hashref()) {
              if ( $item->{'biblionumber'} ) {
                  push @items, $item;
              } else {
                  warn "[Koha::Plugin::BookSlide] - report does not contain biblionumbers, aborting";
                  return;
              }
          }
       } else {
            warn "[Koha::Plugin::BookSlide] - can't find report $report_id";
            return;
       }
    } else {
        warn "[Koha::Plugin::BookSlide] - no source of required records supplied.";
        return;
    }

    foreach my $item ( @items ) {
        my $record = GetMarcBiblio({ biblionumber => $item->{'biblionumber'} });
        next if ! $record;
        my $title;
        my $subtitle = " ";
        my $author;
        my $marcflavour = C4::Context->preference("marcflavour");
        if ($marcflavour eq 'MARC21'){
            $title = $record->subfield('245', 'a');
            $subtitle = $record->subfield('245', 'b');
            $author = $record->subfield('100', 'a');
        }elsif ($marcflavour eq 'UNIMARC') {
            $title = $record->subfield('200', 'a');
        }
        $title =~ s/[,\/\s]+$//;
        $subtitle =~ s/[,\/\s]+$// if $subtitle;
        my $url = getThumbnailUrl( $item->{'biblionumber'}, $record );
        if ( $url ){
            if ( $checkflags && $item->{'flag'} == 1 ) {
                $flagged = 1;
            } else {
                $flagged = 0;
                warn "not flagged\n";
            }

            my %image = ( url => $url, title => $title, subtitle => $subtitle, author => $author, biblionumber => $item->{'biblionumber'}, flagged => $item->{'flag'}, );
            push @images, \%image;
        }else{
            warn "[Koha::Plugin::BookSlide] There was no image found for biblionumber $item->{'biblionumber'} : \" $title \"\n";
        }
    }

    unless ( @images ) {
        warn "[Koha::Plugin::BookSlide] No images were found for virtualshelf '$shelfname' (id: $selectedShelf). OpacMainUserBlock kept unchanged.\n";
        return;
    }

    my $pluginDirectory = C4::Context->config("pluginsdir");
    my $tt = Template->new(INCLUDE_PATH => $pluginDirectory);
    my $mainUserBlockData = "";
    $tt->process('Koha/Plugin/BookSlide/opac-bookslide.tt',
                {   shelfname => $shelfname,
                    documents => \@images,
                    showTitleAndAuthor => $showTitleAndAuthor,
                    flagtext => $flagtext,
                },
                \$mainUserBlockData,
                { binmode => ':utf8' }
                ) || warn "Unable to generate BookSlide, ". $tt->error();
    my $userJSData = "";
    $tt->process('Koha/Plugin/BookSlide/opac-userjs.tt',
                {   slidesToShow => $slidesToShow,
                    autoplay => $autoplay,
                    arrows => $arrows,
                    sliderSpeed => $sliderSpeed,
                },  
                \$userJSData,
                { binmode => ':utf8' }
                ) || warn "Unable to generate userJS ". $tt->error();
    my $preferencedata = {
        "OpacMainUserBlock" => $mainUserBlockData,
        "OPACUserJS"        => $userJSData,
    };

    if ( C4::Context->preference('OpacMainUserBlock') ) {
        $self->insertContent($preferencedata, 0);
    } else {
        $self->insertContent($preferencedata, 1);
    }
}

sub insertContent{
    my ( $self, $data, $news ) = @_;
    my $stmt = $dbh->prepare("select * from systempreferences where variable=?");
    my @contentblocks = ("OPACUserJS", "OpacMainUserBlock");
    my $current_val;

    foreach my $content (@contentblocks) {
        if ( $content eq 'OpacMainUserBlock' && $news ) {
            # Fetch OpacMainUserBlock news items in all languages
            my $opacmainuserblocks = $dbh->prepare("SELECT * from opac_news where lang LIKE '%OpacMainUserBlock%';");
            $opacmainuserblocks->execute();

            # Loop through OpacMainUserBlock news items of all languages
            # Add slider markup to each
            while ( my $news = $opacmainuserblocks->fetchrow_hashref() ) {
                my $href_entry2;
                my $current_news_val;
                my $news_value;

                # Grab existing news item content
                $current_news_val = $news->{content};

                # insert tags to identify the bookslider and make sure it's unique 
                my $first_line = "<!-- Begin bookslide -->";
                my $second_line ="<!-- End bookslide -->";

                if(index($current_news_val, $first_line) == -1 && index($current_news_val, $second_line) == -1  ){
                    # Bookslide tags ($first_line and $second_line) don't exist 
                    # in OpacMainUserBlock news item. Insert the tags and slider markup
                    $current_news_val = $news_value."\n".$first_line.$data->{$content}.$second_line;
                } else {
                    # First and second lines already exist in OpacMainUserBlock news item
                    # replace the content within $first_line and $second_line with 
                    # new slider markup.
                    # Retain other content in OpacMainUserBlock 
                    $news_value = $first_line.$data->{$content}.$second_line;
                    $current_news_val =~ s/$first_line.*?$second_line/$news_value/s;
                }

                # Set OpacMainUserBlock content and idnew values
                $href_entry2->{content} = $current_news_val;
                $href_entry2->{idnew} = $news->{'idnew'};
                upd_opac_new($href_entry2);
            }
            # Close DB connection
            $opacmainuserblocks->finish();
        }
        else {
            # Update OpacUserJS syspref (and OpacMainUserBlock if it exists as a syspref)
            $stmt->execute($content);

            # Grab existing syspref content
            my $value;
            while (my $row = $stmt->fetchrow_hashref()) {
                $current_val =$row->{'value'};
            }
            $stmt->finish();

            # insert tags to identify the bookslider and make sure it's unique 
            my $first_line = "<!-- Begin bookslide -->";
            my $second_line ="<!-- End bookslide -->";
            # first time we'll insert the tags and the data
            if(index($current_val, $first_line) == -1 && index($current_val, $second_line) == -1  ){
                # Bookslide tags don't exist in syspref
                # Insert the tags and slider markup
                $current_val = $value."\n".$first_line.$data->{$content}.$second_line;
            } else {
                # Bookslide tags do exist in syspref
                # First and second lines do not exist in OpacMainUserBlock
                # Insert new slider content between tags
                # Retain other content in the syspref
                $value = $first_line.$data->{$content}.$second_line;
                $current_val =~ s/$first_line.*?$second_line/$value/s;
            }
            C4::Context->set_preference( $content , $current_val );
        }

    }
}

sub retrieveUrlFromGoogleJson {
    my $res = shift;
    my $json = decode_json($res->decoded_content);

    return unless exists $json->{items};
    return unless        $json->{items}->[0];
    return unless exists $json->{items}->[0]->{volumeInfo};
    return unless exists $json->{items}->[0]->{volumeInfo}->{imageLinks};
    return unless exists $json->{items}->[0]->{volumeInfo}->{imageLinks}->{thumbnail};

    return $json->{items}->[0]->{volumeInfo}->{imageLinks}->{thumbnail};
}

sub retrieveUrlFromCoceJson {
    my $res = shift;
    my $json = decode_json($res->decoded_content);

    return unless keys %$json;
    return $json->{$_} for keys %$json;
}

sub getUrlFromExternalSources {
    my $isbn = shift;

    my $coversource = {};

    # add syndetics here
    my $syndeticsID=C4::Context->preference('SyndeticsClientCode');
    $coversource->{SyndeticsCoverImages} = { 
        'priority'=> 1,
        'url' => "http://www.syndetics.com/index.aspx?isbn=$isbn/LC.GIF&client=$syndeticsID&type=xw10&upc=&oclc=",
        'content_length' => 700, # Syndetics sends back a page with 676 bytes of data if a cover can't be found.
    };

    $coversource->{OpacCoce} = {
        'priority' => 2,
        'retrieval' => \&retrieveUrlFromCoceJson,
        'url' => C4::Context->preference('CoceHost').'/cover'
                ."?id=$isbn"
                .'&provider='.join(',', C4::Context->preference('CoceProviders')),
    };

    $coversource->{OPACAmazonCoverImages} = {
        'priority' => 3,
        'url' => "https://images-na.ssl-images-amazon.com/images/P/$isbn.01.MZZZZZZZ.jpg",
        'content_length' => 500,
    };

    $coversource->{GoogleJackets} = {
        'priority' => 4,
        'retrieval' => \&retrieveUrlFromGoogleJson,
        'url' => "https://www.googleapis.com/books/v1/volumes?q=isbn:$isbn&country=CA",
    };

    $coversource->{OpenLibraryCovers} = {
        'priority' => 5,
        'url' => "https://covers.openlibrary.org/b/isbn/$isbn-M.jpg?default=false",
    };

    my $ua = LWP::UserAgent->new;
    my @orderedProvidersByPriority = sort { $coversource->{$a}->{priority} <=> $coversource->{$b}->{priority} } keys %$coversource;

    for my $provider ( @orderedProvidersByPriority ) {

        next if ( $provider eq 'OpacCoce' );

        if ( C4::Context->preference($provider) ) {

            my $url = $coversource->{$provider}->{url};
            my $req = HTTP::Request->new( GET => $url );
            my $res = $ua->request( $req );
            next if !$res->is_success;

            if ( exists $coversource->{$provider}->{content_length} ) {
                next if $res->header('content_length') <= $coversource->{$provider}->{content_length};
            }

            if ( exists $coversource->{$provider}->{retrieval} ) {
                $url = $coversource->{$provider}->{retrieval}->($res);
                next unless $url;
            }

            return $url;

        } # if source enabled by systempreference
    } # foreach providers

    # FIXME: hardcoded fallback to Amazon.com, to be continued... cam#6918
#    my $url;
#    $url = $coversource->{OPACAmazonCoverImages}->{url};
#    my $req = HTTP::Request->new( GET => $url );
#    my $res = $ua->request( $req );
#    return if !$res->is_success;
#    return if $res->header('content_length') <= $coversource->{OPACAmazonCoverImages}->{content_length};
#    return $url;
     return;
}

sub getThumbnailUrl
{
    my $biblionumber = shift;
    my $record = shift;
    return if ! $record;
    my $marcflavour = C4::Context->preference("marcflavour");
    my @isbns;
    if ($marcflavour eq 'MARC21' ){
        @isbns = $record->field('020');
    }elsif($marcflavour eq 'UNIMARC'){
        @isbns = $record->field('010');
    }

    # We look for image locally, if available we return relative path and exit function.
    if ( C4::Context->preference("OPACLocalCoverImages") ) {
        my $stm = $dbh->prepare("SELECT COUNT(*) AS count FROM biblioimages WHERE biblionumber=$biblionumber;");
        $stm->execute();
        if ( $stm->fetchrow_hashref()->{count} > 0 ) {
            return "/cgi-bin/koha/opac-image.pl?thumbnail=1&biblionumber=$biblionumber";
        }
    }

    #If there is not local thumbnail, we look for one on Amazon, Google and Openlibrary in this order and we will exit when a thumbnail is found.
    foreach my $field ( @isbns )
    {
        my $isbn = GetNormalizedISBN( $field->subfield('a') );
        next if ! $isbn;

        return getUrlFromExternalSources($isbn);
    }

    return;
}

#Remove the plugin
sub uninstall() {
    my ( $self, $args ) = @_;
    my $dbh = $dbh;
    my $stmt = $dbh->prepare("select * from systempreferences where variable=?");
    my @content = ("OPACUserJS", "OpacMainUserBlock");
    my $news = 0 ;
    if ( !C4::Context->preference('OpacMainUserBlock') ) {
        $news = 1;
    }

    foreach my $content (@content) {
        #Update OpacMainUserBlock news item
        if ( $content eq 'OpacMainUserBlock' && $news ) {
            my $opacmainuserblocks = $dbh->prepare("SELECT * from opac_news where lang LIKE '%OpacMainUserBlock%';");
            $opacmainuserblocks->execute();
            while ( my $news = $opacmainuserblocks->fetchrow_hashref() ) {
                my $href_entry2;
                my $current_news_val;
                my $news_value;

                $current_news_val = $news->{content};
                my $first_line = "<!-- Begin bookslide -->";
                my $second_line ="<!-- End bookslide -->";
                $current_news_val =~ s/$first_line.*?$second_line//s;

                my $query = $dbh->prepare("update opac_news set content = ? where id = ?");
                $query->execute($current_news_val, $news->{idnew});
                $query->finish();
            }
            #Close the connections to the DB
            $opacmainuserblocks->finish();
        } else {
            $stmt->execute($content);

            my $value;
            while (my $row = $stmt->fetchrow_hashref()) {
                $value ="$row->{'value'}";
            }

            my $first_line = "<!-- Begin bookslide -->";
            my $second_line ="<!-- End bookslide -->";
            $value =~ s/$first_line.*?$second_line//s;

            my $query = $dbh->prepare("update systempreferences set value= ? where variable=?");
            $query->execute($value, $content) ;
            $query->finish();

            #Close the connections to the DB
            $stmt->finish();
        }
    }
}

1;

